Usage: CertificateValidationChecker inputFilePath outputFilePath errorFilePath portsFilePath validateChain(true|false) monthsToExpiry
inputFilePath: path to file that contains the list of IP addresses/ranges and URLs 
outputFilePath: Output of successfully scanned certificates
errorFilePath: output path to store the errors encountered while scanning URLs
portsFilePath: path to input file for ports. Ports listed in this files will be scanned for each IP address/URL
validateChain: flag to set if the entire chain should be validated
monthsToExpiry: Expiry threshold: any certificates exipiring in less than this number will be flagged for renewal
